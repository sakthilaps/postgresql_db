<--PostgreSQL JOINS--->>

select * from contacts;

select * from customers;

A JOIN clause is used to combine rows from two or more tables

<<---INNER JOIN--->>

create table cust (
	customer_id INT primary key,
   customer_name VARCHAR(255) NOT NULL
 );



CREATE TABLE contact(
   contact_id INT GENERATED ALWAYS AS IDENTITY,
   customer_id INT,
   contact_name VARCHAR(255) NOT NULL,
   phone VARCHAR(15),
   email VARCHAR(100),
   PRIMARY KEY(contact_id),
   CONSTRAINT fk_customer
      FOREIGN KEY(customer_id) 
	  REFERENCES customers(customer_id)
);





INSERT INTO customers(customer_name)
VALUES('aaaaaa'),
      ('bbbbbbb'),
	  ('ccccccc'),
	  ('dddddd');
select * from customers


INSERT INTO contacts(customer_id, contact_name, phone, email)
VALUES(1,'John Doe','(408)-111-1234','john.doe@bluebird.dev'),
      (1,'Jane Doe','(408)-111-1235','jane.doe@bluebird.dev'),
      (2,'David Wright','(408)-222-1234','david.wright@dolphin.dev'),
	  (3,'prem Wright','(408)-333-1234','prem.wright@dolphin.dev'),
	  (6,'Salim','(408)-666-1234','salim@dolphin.dev');
	  
select * from contacts


<<--Inner join--->>

select contact_id,contact_name,customer_name from customers inner join contacts
on contacts.customer_id = customers.customer_id


<<---left join--->>

select contact_id,contact_name,customer_name,phone from customers left join contacts
on contacts.customer_id = customers.customer_id


<<--Right join-->>

select contact_id,contact_name,customer_name,phone from customers right join contacts
on contacts.customer_id = customers.customer_id


<<---Full join-->>
select contact_id,contact_name,customer_name,phone from customers full join contacts
on contacts.customer_id = customers.customer_id


<<---cross join-->>
select contact_id,contact_name,customer_name,phone from customers cross join contacts


<<<---Union -->>
select customer_id from customers union 
select contact_id from contacts


<<--Union All-->>

select customer_id, customer_name from customers 
union all select contact_id, contact_name from contacts order by customer_id


=====================================================================================


<<---PostgreSQL GROUP BY Clause--->>


select * from employee

select job_des, sum(salary) from employee
group by job_des order by sum(salary)


<<--Having clause-->>

select job_des, sum(salary)  from employee
group by job_des having sum(salary)>60000 


<<--PostgreSQL EXISTS Operator-->>

select * from customers

select * from contacts


SELECT customers.customer_name
FROM customers
WHERE EXISTS (
  SELECT contact_id
  FROM contacts
  WHERE customer_id = customers.customer_id
);


<<--PostgreSQL NOT EXISTS Operator-->>



SELECT customers.customer_name
FROM customers
WHERE NOT EXISTS (
  SELECT contact_id
  FROM contacts
  WHERE customer_id = customers.customer_id
);



<<--PostgreSQL ANY Operator-->

The ANY operator allows you to perform a comparison between a single column value and a range of other values

SELECT emp_id
  FROM employee
  WHERE salary > 40000


select * from employee

SELECT emp_name
FROM employee
WHERE emp_id = ANY (
  SELECT emp_id
  FROM employee
  WHERE salary > 40000
);



<<--PostgreSQL ALL Operator-->
select * from customers

select * from contacts


select customer_id from customers where customer_name in ('aaaaaa','bbbbbbb')

select * from contacts
where customer_id<> All
(select customer_id from customers where customer_name in ('aaaaaa','bbbbbbb'))
































