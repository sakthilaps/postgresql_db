select * from product_groups;

select * from products;


<<---LAG and LEAD functions-->>

<<--LAG functions--->>
The LAG() function has the ability to access data from the previous row

select group_name, product_name, price ,lag(price,1)
over(partition by group_name order by price) as prev_price,
price-lag(price,1)
over(partition by group_name order by price) as cur_prev_diff from products 
inner join product_groups using(group_id);



<<--Lead functions--->>



The LEAD() function can access data from the next row

select group_name, product_name, price ,lead(price,1)
over(partition by group_name order by price) as next_price,
price-lead(price,1)
over(partition by group_name order by price) as cur_next_diff from products 
inner join product_groups using(group_id);


<---PostgreSQL CUME_DIST() --->>

The CUME_DIST() function returns the cumulative distribution of a value within a set of values.



CREATE TABLE sales_stats(
    name VARCHAR(100) NOT NULL,
    year SMALLINT NOT NULL CHECK (year > 0),
    amount DECIMAL(10,2) CHECK (amount >= 0),
    PRIMARY KEY (name,year)
);




INSERT INTO 
    sales_stats(name, year, amount)
VALUES
    ('John Doe',2018,120000),
    ('Jane Doe',2018,110000),
    ('Jack Daniel',2018,150000),
    ('Yin Yang',2018,30000),
    ('Stephane Heady',2018,200000),
    ('John Doe',2019,150000),
    ('Jane Doe',2019,130000),
    ('Jack Daniel',2019,180000),
    ('Yin Yang',2019,25000),
    ('Stephane Heady',2019,270000);
	
	
	
select * from sales_stats


select name, year, amount,
cume_dist() over (order by amount)
from sales_stats where year = 2018

select name, year, amount,
cume_dist() over (order by amount), round(cume_dist() over (order by amount)::numeric * 100, 2) as cume_dis_precentage
from sales_stats; 

<<---PostgreSQL NTILE() function--->>

select * from sales_stats;

select name, year, amount,
ntile(3) over(order by amount)
from sales_stats;


<<--PostgreSQL NTH_VALUE() -->>
The NTH_VALUE() function returns a value from the nth row in an ordered partition of a result set.

select * from products;

SELECT 
    product_id,
    product_name,
    price,
    NTH_VALUE(product_name, 2) 
    OVER(
        ORDER BY price DESC
        RANGE BETWEEN 
            UNBOUNDED PRECEDING AND 
            UNBOUNDED FOLLOWING
    )
FROM 
    products;
	
	
	
SELECT 
    product_id,
    product_name,
    price,
    group_id,
    NTH_VALUE(product_name, 2) 
    OVER(
        PARTITION BY group_id
        ORDER BY price DESC
        RANGE BETWEEN 
            UNBOUNDED PRECEDING AND 
            UNBOUNDED FOLLOWING
    )
FROM 
    products;

<<--PostgreSQL PERCENT_RANK() function--->>

PERCENT_RANK() function to calculate the sales percentile of each employee in 2019:
	
SELECT  name,amount,PERCENT_RANK() OVER (ORDER BY amount)
FROM sales_stats
WHERE year = 2019;	
	
	

	




