create table report_detail(
	report_detail_id int not null,
	report_id int,
	report_column_name varchar(50),
	report_column_value varchar(50),
	status varchar(50),
	created_date date NOT null,
	updated_date date,
	created_by varchar(50) not null,
	updated_by varchar(50),
	foreign key(report_id) references report_header(report_id)
	
);
