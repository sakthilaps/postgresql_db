create table report_header(
	report_id int primary key,
	report_name varchar(50),
	status varchar(50),
	user_id int,
	created_date date NOT null,
	updated_date date,
	created_by varchar(50) not null,
	updated_by varchar(50)
	
);

