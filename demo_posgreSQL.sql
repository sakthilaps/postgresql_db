<<---create table-->>

create table students(
	s_id int NOT NULL,
	s_name varchar(25),
	dept varchar(25),
	s_percent int,
	adder varchar(25)
);	


<<--insert data-->>

insert into students(s_id,s_name,dept,s_percent,adder)
values
(1, 'anura', 'csc', 90, 'chennai'),
(2, 'lakshana', 'ece', 80, 'pune'),
(3, 'ravi', 'eee', 70, 'kerala'),
(4, 'sham', 'csc', 80, 'chennai'),
(5, 'siva', 'ece', 85, 'delhi'),
(6, 'bala', 'mech', 92, 'bigar'),
(7, 'dhavin', 'civil', 73, 'delhi'),
(8,'siva', 'ece', 87, 'mumbai'),
(9, 'ravi', 'chemical',95 , 'delhi'),
(10, 'mani', 'mech', 65, 'banglore')


;

<<--select data-->>

select * from students;


<<---PostgreSQL Operators--->>

1) Equal To:

<<-- We can operate with different operators in the WHERE clause -->>

select * from students where s_id = 9;

2)Less Than:

The < operator is used when you want to return all records where a column is less than a specified value.

select * from students where s_percent<70;

3) greater than:

select * from students where s_percent>90;

4) Less Than or Equal To:

select * from students where s_percent<=80;

5) Greater Than or Equal to:

select * from students where s_percent>=90;

6) Not Equal To:

select * from students where s_name<>'anura';

7)LIKE:

The LIKE operator is used when you want to return all records where a column is equal to a specified pattern.

select * from students;

select * from students where s_name like 's%';


select * from students where s_name like '%i';

select * from students where s_name like '%_n_%';


select * from students where s_name ilike 's%';


8) And operator:

select * from students where dept = 'ece' and s_percent = 85;

9)Or operator:

select * from  students where s_name='ravi' or adder='chennai';

10) In operator:

select * from students where adder in('chennai', 'pune', 'mumbai');


11) Between operator:

select * from students where s_id between 4 and 8;


12) IS Null operator:

select * from students 
where adder is null;

13)Not like:

select * from students where s_name not like 'a%';

14) Not between:

select * from students where s_id not between 4 and 8;



<<--PostgreSQL Select Data-->>

select * from products;

select product_name, price from products;


<<--SELECT DISTINCT--->>

select * from students;

select distinct(s_name), s_percent, dept from students;


<<--WHERE clause-->>

The WHERE clause is used to filter records

select * from employee;


select * from employee where job_des = 'HR'

select * from employee where salary<40000

<<---ORDER BY-->>

select * from sales_stats

select * from sales_stats order by name;

select * from sales_stats order by amount desc;

select * from sales_stats order by amount ;


<<--LIMIT Clause-->>

select * from iris limit 25 


select * from students

select * from students 
limit 6 offset 4 

<<--aggregate function-->>

1) MIN and MAX Functions:

select * from students;

select min(s_percent) from students;


select max(s_percent) as max_per from students;


2)COUNT Function:

select count(s_id) from students; 


select count(s_id) from students
where adder='chennai'; 

3)SUM Function:

select * from employee;

select sum(salary) from employee;

======================================
copy iris from '/home/sakthi/iris.csv' csv header;

select * from iris;
========================================

select sum(sepal_length) from iris
where variety = 'Setosa';

4)AVG Function:


select avg(salary) from employee;

select * from iris;

select avg(petal_length) from iris
where variety = 'Versicolor';


<<--- IN Operato--->>

select * from employee
where emp_id in(select s_id from students )

select * from iris 
where variety not in ('Setosa','Versicolor')



<<---PostgreSQL AS--->>

select s_id as id from students

select * from students


select s_name || s_percent as s_del from students





























































































