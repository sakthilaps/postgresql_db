<<---PostgreSQL Primary Key--->>

A primary key is a column or a group of columns used to identify a row uniquely in a table.

create table po_header(
	po_no int primary key,
	vendor_no int,
	description text,
	shipping_address text
);


<<--How to define primary key constraint on one or two columns-->>
In case the primary key consists of two or more columns, you define the primary key constraint as follows.

create table po_items(
	po_no int,
	item_no int,
	product_no int,
	qty int,
	net_price numeric,
	primary key(po_no,item_no)
	
);


<<--Define primary key when changing the existing table structure -->>

create table product(
	product_no int,
	description text,
	product_cost int
);


alter table product 
add primary key(product_no);


<<---How to add an auto-incremented primary key to an existing table-->

create table vendors (name VARCHAR(255));



INSERT INTO vendors (NAME)
VALUES
	('Microsoft'),
	('IBM'),
	('Apple'),
	('Samsung');


ALTER TABLE vendors ADD COLUMN ID SERIAL PRIMARY KEY;


select * from vendors;


<<---PostgreSQL Foreign Key --->>

A foreign key is a column or a group of columns in a table that reference the primary key of another table.

CREATE TABLE customers(
   customer_id INT GENERATED ALWAYS AS IDENTITY,
   customer_name VARCHAR(255) NOT NULL,
   PRIMARY KEY(customer_id)
);



CREATE TABLE contacts(
   contact_id INT GENERATED ALWAYS AS IDENTITY,
   customer_id INT,
   contact_name VARCHAR(255) NOT NULL,
   phone VARCHAR(15),
   email VARCHAR(100),
   PRIMARY KEY(contact_id),
   CONSTRAINT fk_customer
      FOREIGN KEY(customer_id) 
	  REFERENCES customers(customer_id)
);


INSERT INTO customers(customer_name)
VALUES('BlueBird Inc'),
      ('Dolphin LLC');	   
	  

select * from customers;


INSERT INTO contacts(customer_id, contact_name, phone, email)
VALUES(1,'John Doe','(408)-111-1234','john.doe@bluebird.dev'),
      (1,'Jane Doe','(408)-111-1235','jane.doe@bluebird.dev'),
      (2,'David Wright','(408)-222-1234','david.wright@dolphin.dev');
	  
	  
	  
select * from contacts;	  

<<---super key-->>

Super key is the key that can uniquely identify any row in a database. 

select * from employee;

{emp_id},{emp_id,emp_name},
{emp_id,job_des},{job_des,salary}

<<--Candidate Keys-->>

Candidate Keys are super keys with the least number of columns.

select * from  employee;

{emp_id},{job_des,salary}


<<--Alternate Keys-->>

ALTERNATE KEY is a candidate key which consists of one or more columns used to identify each row uniquely which is not treated as a Primary key.

select * from employee;

{job_des,salary}

<<--unique key-->>

A unique key is a set of one or more than one fields/columns of a table that uniquely identify a record in a database table. 
You can say that it is little like primary key but it can accept only one null value and it cannot have duplicate values.


	  
select * from contacts;	

{contact_name,phone}


<<--sub query-->>
In SQL a Subquery can be simply defined as a query within another query
We can place the Subquery in a number of SQL clauses: WHERE clause, HAVING clause, FROM clause.

	  
select * from employee;	  

select emp_name,job_des,salary from employee 
where salary >(select avg(salary) from employee);
	  

(select avg(salary) from employee);

select * from customers;

select * from contacts;
	  
	  
select contact_id, customer_name, contact_name,email from contacts inner join customers 
on contacts.customer_id = customers.customer_id
WHERE email ='john.doe@bluebird.dev';
	  
	  
























