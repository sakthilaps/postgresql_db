CREATE TABLE product_groups (
	group_id serial PRIMARY KEY,
	group_name VARCHAR (25) NOT NULL
);


CREATE TABLE products (
	product_id serial PRIMARY KEY,
	product_name VARCHAR (255) NOT NULL,
	price DECIMAL (11, 2),
	group_id INT NOT NULL,
	FOREIGN KEY (group_id) REFERENCES product_groups (group_id)
);



INSERT INTO product_groups (group_name)
VALUES
	('Smartphone'),
	('Laptop'),
	('Tablet');


INSERT INTO products (product_name, group_id,price)
VALUES
	('Microsoft Lumia', 1, 200),
	('HTC One', 1, 400),
	('Nexus', 1, 500),
	('iPhone', 1, 900),
	('HP Elite', 2, 1200),
	('Lenovo Thinkpad', 2, 700),
	('Sony VAIO', 2, 700),
	('Dell Vostro', 2, 800),
	('iPad', 3, 700),
	('Kindle Fire', 3, 150),
	('Samsung Galaxy Tab', 3, 200);
	

select*from product_groups


select * from products;
	
<<--inner join-->
SELECT product_id, product_name, price
FROM product_groups
INNER JOIN products ON product_groups.group_id = products.group_id;


<<--left join-->
SELECT product_id, group_name, product_name, price
FROM product_groups
left JOIN products ON product_groups.group_id = products.group_id;


<<--rigth join-->
SELECT product_id, group_name, product_name, price
FROM product_groups
right JOIN products ON product_groups.group_id = products.group_id;


<<---full join--->>
SELECT product_id, group_name, product_name, price
FROM product_groups
full JOIN products ON product_groups.group_id = products.group_id order by price;



<<--UNION operator-->
SELECT group_id, group_name
FROM product_groups
UNION
SELECT product_id, product_name
FROM products;



<<--UNION ALL operator-->
SELECT group_id, group_name
FROM product_groups
UNION ALL
SELECT product_id, product_name
FROM products;


<---Group By--->>

create table employee(
	emp_id int NOT NULL,
	emp_name varchar(50),
	job_des varchar(50),
	salary int
);


insert into employee(emp_id, emp_name, job_des, salary)
values(1, 'Nila', 'Admin', 10000),
(2, 'Anura', 'HR', 20000),
(3, 'Pari', 'Manager', 40000),
(4, 'Mani', 'Admin', 45000),
(5, 'Sara', 'HR', 50000),
(6, 'Krish', 'Manager', 60000),
(7, 'Laks', 'Analyst', 50000),
(8, 'Bala', 'Developer', 30000),
(9, 'Ravi', 'Analyst', 50000),
(10, 'Cheran', 'Developer', 30000)


select * from employee

select job_des, avg(salary) from employee
group by job_des;

<<--- Group by--->>

select job_des, count(emp_id) from employee
group by job_des;


select job_des, count(emp_id) from employee
group by job_des having count(emp_id)>1 order by job_des ;

<<--having-->>


select job_des,count(emp_id) from employee where salary>30000
group by job_des having count(emp_id)>1 order by job_des ;


<<---Exists--->>

select * from product_groups

select * from products


SELECT product_groups.group_name
FROM product_groups
WHERE EXISTS (
  SELECT product_id
  FROM products
  WHERE group_id = product_groups.group_id
);

<<<---Any-->>>
SELECT group_name
FROM product_groups
WHERE group_id = ANY (
  SELECT group_id
  FROM products
  WHERE price > 400
);


<<---All-->>
ALL means that the condition will be true only if the operation is true for all values in the range.

SELECT group_name
FROM product_groups
WHERE group_id = ALL (
  SELECT group_id
  FROM products
  WHERE price<400
);

	

	

	

	

	

	

	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	













