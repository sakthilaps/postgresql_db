<<--Insert data into table-->>

insert into student(s_id,s_name,s_class,s_mark,s_gender)
values
(1, 'anura', 'csc', 90, 'female'),
(2, 'lakshana', 'ece', 80, 'female'),
(3, 'ravi', 'eee', 70, 'male'),
(4, 'sham', 'csc', 80, 'male'),
(5, 'siva', 'ece', 85, 'female')
;


<<--Select Data-->>
select *from student

select s_name,s_mark from student;

<<---UPDATE statement is used to modify the value(s) in existing records--->
.update student set s_name='balu' where s_id =4; 

select *from student

<---PostgreSQL ADD COLUMN--->>
alter table student
add column s_date date;


select *from student


<<---PostgreSQL DROP COLUMN--->>
alter table student
drop column s_date;

select *from student



